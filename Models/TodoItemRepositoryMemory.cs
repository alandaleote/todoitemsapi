using System;
using System.Collections.Generic;
using System.Collections.Concurrent;

namespace TodoApi
{
    public class TodoItemRepositoryMemory : ITodoItemRepository
    {
        private readonly ConcurrentDictionary<Guid, TodoItem> data = new ConcurrentDictionary<Guid, TodoItem>();

        public TodoItemRepositoryMemory()
        {
            add(new TodoItem());
        }

        public TodoItem add(TodoItem todo)
        {
            todo.Id = Guid.NewGuid();
            if (data.TryAdd(todo.Id, todo))
            {
                return todo;
            }
            throw new Exception("Erro, Id já existente.");
        }

        public IEnumerable<TodoItem> getAll()
        {
            return data.Values;
        }

        public TodoItem getById(Guid id)
        {
            TodoItem todoitem;
            data.TryGetValue(id, out todoitem);
            return todoitem;
        }

        TodoItem ITodoItemRepository.remove(Guid id)
        {
            TodoItem todoitem;
            data.TryRemove(id, out todoitem);
            return todoitem;
        }

        public void update(TodoItem todo)
        {
            try
            {
                data[todo.Id] = todo;
            }
            catch (System.Exception)
            {
                
                throw new Exception("Id não encontrado.");;
            }
            
        }

    }
}
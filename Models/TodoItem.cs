using System;
using System.ComponentModel.DataAnnotations;

namespace TodoApi
{
    public class TodoItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        [Required]
        [StringLength(25)]
        public string Description { get; set; }
        public bool IsComplete { get; set; }
    }

}
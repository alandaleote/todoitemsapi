using System;
using System.Collections.Generic;

namespace TodoApi
{
    public interface ITodoItemRepository
    {
        IEnumerable<TodoItem> getAll();
        TodoItem getById(Guid id);
        TodoItem add(TodoItem todo);
        TodoItem remove(Guid id);
        void update(TodoItem todo);

    }
}
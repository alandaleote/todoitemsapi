using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace TodoApi.Controllers
{
    [ApiController]
    [Route("api/TodoItems")]

    public class TodoController : ControllerBase
    {
        private readonly ILogger<TodoController> _logger;
        private readonly ITodoItemRepository _repository;

        public TodoController(ILogger<TodoController> logger, ITodoItemRepository repository)
        {
            _logger = logger;
            _repository = repository;
        }

        [HttpGet]
        //GET /api/TodoItems
        public IEnumerable<TodoItem> GetAll()
        {
            return _repository.getAll();
        }

        [HttpGet("{id}")]
        //GET /api/TodoItems/{id}
        public TodoItem GetById(Guid id)
        {
            _logger.LogInformation($"Guid = {id}");
            return _repository.getById(id);
        }

        [HttpPost]
        //POST /api/TodoItems
        public ActionResult<TodoItem> PostNewTodo([FromBody] TodoItem todoitem)
        {
            var newtodoitem = _repository.add(todoitem);
            return CreatedAtAction(nameof(GetById), new {id = newtodoitem.Id});
        }

        [HttpPut("{id}")]
        //PUT /api/TodoItems/{id}
        public IActionResult PutTodoItem(Guid id, [FromBody] TodoItem todoitem)
        {
            if (id != todoitem.Id)
            {
                return BadRequest();
            }
            else if (_repository.getById(id) == null)
            {
                return NotFound();
            }
            _repository.update(todoitem);
            return NoContent();
        }

        [HttpDelete("{id}")]
        //DELETE /api/TodoItems/{id}
        public ActionResult<TodoItem> DeleteTodoItem(Guid id)
        {
            var todoitem = _repository.remove(id);
            if (todoitem != null)
            {
                return todoitem;
            }
            return NotFound();
        }
    }
}